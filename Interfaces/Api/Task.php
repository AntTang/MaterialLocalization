<?php
/**
 * @package     Task.php
 * @author      Jing Tang <tangjing3321@gmail.com>
 * @link        http://www.slimphp.net/
 * @version     2.0
 * @copyright   Copyright (c) http://www.slimphp.net
 * @date        2017年7月28日
 */

namespace MaterialLocalization\Interfaces\Api;

/**
 * 素材下载任务
 * 
 * @author Jing Tang <tangjing3321@gmail.com>
 */
interface Task
{
    /**
     * @api {post} /api/create/{id} 创建下载任务
     * @apiName create
     * @apiGroup Task
     * @apiVersion 1.0.0
     * @apiDescription 创建下载任务
     *
     * @apiParam {int} id 内容id
     * @apiParam {array} [material_info] 图片素材等等地址
     * @apiParam {string} [url] 视频素材地址
     *
     * @apiSuccess {String} code 结果码
     * @apiSuccess {String} code.0 错误码 成功
     * @apiSuccess {String} code.1001 错误码 非法字段
     * @apiSuccess {String} code.1002 错误码 创建下载任务失败
     * @apiSuccess {String} code.1003 错误码 素材无变化，更新下载任务失败
     * @apiSuccess {String} msg 消息说明
     * @apiSuccess {Object} result 数据封装
     * @apiSuccess {Object} result.insert_id 任务id
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *   "code": 0,
     *   "msg": "success",
     *   "result": {
     *     "insert_id": "50"
     *   }
     * }
     * @apiSuccessExample Error-Response:
     * HTTP/1.1 200 OK
     * {
     *   "code": 1003,
     *   "msg": "素材无变化，更新下载任务失败"
     * }
     */
    public function create();
    
    /**
     * @api {get} /api/status/{id} 检查任务状态
     * @apiName status
     * @apiGroup Task
     * @apiVersion 1.0.0
     * @apiDescription 检查任务状态
     *
     * @apiParam {int} id 内容id
     *
     * @apiSuccess {String} code 结果码
     * @apiSuccess {String} code.0 错误码 成功
     * @apiSuccess {String} code.1004 错误码 任务不存在
     * @apiSuccess {String} msg 消息说明
     * @apiSuccess {Object} result 数据封装
     * @apiSuccess {Object} result.status 任务状态 0：未下载，1：下载中，2：下载完成，
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *   "code": 0,
     *   "msg": "success",
     *   "result": {
     *     "status": "0"
     *   }
     * }
     * @apiSuccessExample Error-Response:
     * HTTP/1.1 200 OK
     * {
     *   "code": 1004,
     *   "msg": "任务不存在"
     * }
     */
    public function status();
}