<?php
/**
 * @package     routes.php
 * @author      Jing <tangjing3321@gmail.com>
 * @link        http://www.slimphp.net
 * @version     1.0
 * @copyright   Copyright (c) SlimCustom.
 * @date        2017年5月2日
 */
namespace MaterialLocalization\bootstrap;

use SlimCustom\Libs\App as Router;
use MaterialLocalization\Controllers\Api\V1\Task;
use SlimCustom\Libs\Middlewares\Version;

// 接口
$group = Router::group('/api', function () {
    $this->post('/task/create/{id}', Task::class . ':create'); // 创建任务
    $this->get('/task/status/{id}', Task::class . ':status'); // 查询任务状态
});
// 接口版本实现
$group->add(Version::class . ':resolve');