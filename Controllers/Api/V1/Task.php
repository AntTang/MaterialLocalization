<?php
/**
 * @package     Task.php
 * @author      Jing Tang <tangjing3321@gmail.com>
 * @link        http://www.slimphp.net/
 * @version     2.0
 * @copyright   Copyright (c) http://www.slimphp.net
 * @date        2017年7月21日
 */
namespace MaterialLocalization\Controllers\Api\V1;

use SlimCustom\Libs\Controller\Api;

/**
 * 素材下载任务
 *
 * @author Jing Tang <tangjing3321@gmail.com>
 */
class Task extends Api
{
    /**
     * 创建下载任务
     *
     * @param integer $id            
     * @return \SlimCustom\Libs\Http\Response
     */
    public function create($id)
    {
        $pairs = [
            'content_id' => $id,
            'material_info' => json_encode(request()->getParam('material_info', [])),
            'url' => request()->getParam('url', ''),
            'status' => 0
        ];
        return response()->success([
            'insert_id' => model('Task')->create($pairs)
        ]);
    }

    /**
     * 检查任务状态
     *
     * @param integer $id            
     * @return \SlimCustom\Libs\Http\Response
     */
    public function status($id)
    {
        $status = model('Task')->where('content_id', '=', $id)->rules(['id' => 'required|integer'], ['id' => $id])->row(['status']);
        if (! isset($status['status'])) {
            return response()->error(1004);
        }
        return response()->success($status);
    }
}