<?php
/**
 * @package     apis.php
 * @author      Jing Tang <tangjing3321@gmail.com>
 * @link        http://www.slimphp.net/
 * @version     2.0
 * @copyright   Copyright (c) http://www.slimphp.net
 * @date        2017年7月25日
 */

return [
    // 静态文件地址
    'static_path' => App::$instance->publicPath() . 'static',
    // 应用id,key
    'appid' => 1,
    'appkey' => 'vr6D2celv07tGwNhHQpXXWDtHU8eKr4J',
    // api
    'api' => [
        // 通知生产端发布素材
        'confirmPush' => 'http://mxuapi-team.cloud.hoge.cn/api/content/confirmPush'
    ]
];