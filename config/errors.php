<?php
/**
 * @package     errors.php
 * @author      Jing <tangjing3321@gmail.com>
 * @link        http://www.slimphp.net
 * @version     1.0
 * @copyright   Copyright (c) SlimCustom.
 * @date        2017年5月15日
 */

return [
    1001 => [
        'zh' => '非法字段',
        'en' => 'Illegal field'
    ],
    1002 => [
        'zh' => '创建下载任务失败',
        'en' => 'Create task field'
    ],
    1003 => [
        'zh' => '素材无变化，更新下载任务失败',
        'en' => 'No change material, update download failed'
    ],
    1004 => [
        'zh' => '任务不存在',
        'en' => 'Not existed task'
    ],
    1005 => [
        'zh' => '批量处理失败',
        'en' => 'Batch processing failed'
    ],
];