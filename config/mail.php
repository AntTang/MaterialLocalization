<?php
/**
 * @package     mail.php
 * @author      Jing Tang <tangjing3321@gmail.com>
 * @link        http://www.slimphp.net/
 * @version     2.0
 * @copyright   Copyright (c) http://www.slimphp.net
 * @date        2017年8月16日
 */

return [
    // 是否启用邮件报告
    'disable' => false,
    
    // 发送时间间隔
    'interval' => 3600,
    
    // 发送人
    'from' => 'xx <xx@xx.com>',
    
    // 收件人
    'to' => 'xx@xx.com',
    
    // 主题
    'subject' => '素材本地化错误报告',
    
    // stmp服务器
    'host' => 'smtp.qq.com',
    
    // 用户名
    'username' => '',
    
    // 密码
    'password' => ''
];