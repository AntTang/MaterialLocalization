<?php
/**
 * @package     Localization.php
 * @author      Jing Tang <tangjing3321@gmail.com>
 * @link        http://www.slimphp.net/
 * @version     2.0
 * @copyright   Copyright (c) http://www.slimphp.net
 * @date        2017年6月7日
 */

use SlimCustom\Libs\App;
use MaterialLocalization\Repositories\Localization;

$application = require __DIR__ . '/../../SlimCustom/index.php';
$application->setName('MaterialLocalization')->setPath(realpath(__DIR__ . '/../'))->boot();
$daemon = daemon();

/**
 * 素材本地化任务
 */
$daemon->group('Localization', function () {
    // 内容关联素材下载
    $this->call(10, 'Download', function () {
        set_time_limit(3600 * 6);
        while (true) {
            App::single(Localization::class)->download();
            sleep(1);
        }
        exit;
    });
    
    // 通知生产端修改发布状态
    $this->call(10, 'Notify', function () {
        $localization = App::single(Localization::class);
        $seconds = 0;
        while (true) {
            if (! $localization->notify()) {
                $seconds++;
                $localization->errorReportMail("回调通知任务失败{$seconds}次，请上服务器排查日志");
                sleep($seconds);
            }
            else {
                sleep(1);
            }
        }
        exit;
    });
    
    // 日志裁剪
    $this->call(10, 'CutLogs', function () {
        while (true) {
            $log = config('logger.path', '');
            if (@filesystem()->size($log) >= 1073741842) {
                filesystem()->delete($log);
            }
            sleep(3600);
        }
        exit;
    });
    
});

// 运行任务
$daemon->run();